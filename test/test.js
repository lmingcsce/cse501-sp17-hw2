///<reference path="../src/q.ts"/>
mocha.ui("bdd");
function cleanData() {
    var out = [];
    for (var i = 0; i < window.rawData.length; i++) {
        var x = window.rawData[i];
        out.push({ type: x[13], description: x[15], date: x[17], area: x[19] });
    }
    return out;
}
describe("Problem 1", function () {
    it("Executing queries", function () {
        var q = new ThenNode(new AllNode(), new FilterNode(function (x) { return x[0] % 2 == 1; }));
        var out = q.execute(window.rawData);
        var good = [];
        for (var i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][0] % 2) {
                good.push(window.rawData[i]);
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.deep.equal(good);
    });
    it("Write a query", function () {
        var good1 = [];
        var good2 = [];
        for (var i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][13].match(/THEFT/)) {
                good1.push(window.rawData[i]);
            }
            if (window.rawData[i][13].match(/^VEH-THEFT/)) {
                good2.push(window.rawData[i]);
            }
        }

        var out1 = theftsQuery.execute(window.rawData);
        var out2 = autoTheftsQuery.execute(window.rawData);
        chai.expect(out1).to.deep.equal(good1);
        chai.expect(out2).to.deep.equal(good2);
    });
    it("Add Apply and Count nodes", function () {
        function even(x) {
            return (x % 2) === 0;
        }
        var q = new ThenNode(new ApplyNode(function (x) { return x[0]; }), new ThenNode(new FilterNode(even), new CountNode()));
        var out = q.execute(window.rawData);

        var good = 0;
        for (var i = 0; i < window.rawData.length; i++) {
            if (even(window.rawData[i][0]))
                good++;
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });
    it("Clean the data", function () {
        var out = cleanupQuery.execute(window.rawData);
        chai.expect(out).to.deep.equal(cleanData());
    });
    it("Implement a call-chaining interface", function () {
        var q = Q.apply(function (x) { return x[0] % 2; })
            .filter(function (x) { return x; })
            .count();

        var out = q.execute(window.rawData);
        var good = 0;
        for (var i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][0] % 2)
                good++;
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });
    it("Reimplement queries with call-chaining", function () {
        var data = cleanData();
        var out = cleanupQuery2.execute(window.rawData);

        chai.expect(out).to.deep.equal(data);

        var good1 = [];
        var good2 = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].type.match(/THEFT/)) {
                good1.push(data[i]);
            }
            if (data[i].type.match(/^VEH-THEFT/)) {
                good2.push(data[i]);
            }
        }
        var out1 = theftsQuery2.execute(data);
        var out2 = autoTheftsQuery2.execute(data);

        chai.expect(out1).to.deep.equal(good1);
        chai.expect(out2).to.deep.equal(good2);
    });
});
describe("Problem 2", function () {
    it("Optimize filters", function () {
        var q = Q.filter(function (x) { return x; }).filter(function (x) { return x; });
        var q2 = q.optimize();

        chai.expect(q2.type).to.equal("Then");
        chai.expect(q2.first.type).to.equal("All");
        chai.expect(q2.second.type).to.equal("Filter");
    });
    it("Internal node types and CountIf", function () {
        var q = Q.filter(function (x) { return x[0] % 2; }).count();
        var q2 = q.optimize();
        chai.expect(q2.type).to.equal("Then");
        chai.expect(q2.first.type).to.equal("All");
        chai.expect(q2.second.type).to.equal("CountIf");
    });
});
describe("Problem 3", function () {
    it("Cartesian products", function () {
        var q = new CartesianProductNode(new AllNode(), new AllNode());
        var out = q.execute(window.rawData);
        var l = window.rawData.length;
        chai.expect(out).to.have.length(l * l);
    });
    it("Joins", function () {
        var q = Q.join(Q, function (l, r) { return l[0] == r[0] - 1; }).apply(function (x) { return x[0]; });
        var out = q.execute(window.rawData);
        var l = window.rawData.slice(1);
        chai.expect(out).to.deep.equal(l.map(function (x) { return x[0]; }));
    });
    it("Joins Objects", function () {
        var q = Q.join(Q, function (l, r) { return true; });
        var data = [{ a: 0, b: 2 }];
        var expected = [{ a: 0, b: 2 }];
        chai.expect(q.execute(data)).to.deep.equal(expected);
        chai.expect(q.optimize().execute(data)).to.deep.equal(expected);
    });
    it("Optimizing joins", function () {
        var q = Q.join(Q, function (l, r) {
            return l[0] == r[0] - 1;
        });
        var outQ = q.optimize();
        chai.expect(outQ.type).to.equal("Join");
        var out = outQ.execute(window.rawData);
        chai.expect(out).to.deep.equal(q.execute(window.rawData));
    });
    it("Joins on fields", function () {
        var q = Q.join(Q, "type").count();
        var out = q.execute(cleanData());
        var good = 0;
        for (var i = 0; i < window.rawData.length; i++) {
            for (var j = 0; j < window.rawData.length; j++) {
                if (window.rawData[i][13] == window.rawData[j][13])
                    good++;
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });
    it("Implement hash joins", function () {
        var q = new HashJoinNode("type", new AllNode(), new AllNode()).count();
        var out = q.execute(cleanData());
        var good = 0;
        for (var i = 0; i < window.rawData.length; i++) {
            for (var j = 0; j < window.rawData.length; j++) {
                if (window.rawData[i][13] == window.rawData[j][13])
                    good++;
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });
    it("Allows duplicate records in HashJoin", function () {
        var q = Q.join(Q, "a");
        var datum = { a: 3, b: 2 };
        var data = [datum, datum];
        chai.expect(q.execute(data)).to.have.length(4);
        chai.expect(q.optimize().execute(data)).to.have.length(4);
    });
    it("Optimize joins on fields to hash joins", function () {
        var q = Q.join(Q, "type");
        var out = q.optimize();
        chai.expect(out.type).to.equal("HashJoin");
    });
});
describe("Problem 4", function () {
    it("Find thefts that happens in SW with record number less than thefts that happens in NW",function () {
        var q = Q.filter(function (x) { return x[13].indexOf("THEFT") >= 0; });
        var q1 = q.filter(function (x) { return x[19].indexOf("SW") >= 0; });
        var q2 = q.filter(function (x) { return x[19].indexOf("NW") >= 0; });
        var q3 = q1.join(q2, function (l, r) {return l[0] < r[0];});
        console.log(q3.execute(window.rawData));
    });
});
//# sourceMappingURL=test.js.map
