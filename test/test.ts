///<reference path="../src/q.ts"/>
mocha.ui("bdd");

function cleanData() {
    let out = [];
    for (let i = 0; i < window.rawData.length; i++) {
        const x = window.rawData[i];
        out.push({type: x[13], description: x[15], date: x[17], area: x[19]});
    }
    return out;
}

describe("Problem 1", function () {
    it("Executing queries", function () {
        const q = new ThenNode(
            new AllNode(),
            new FilterNode(x => x[0] % 2 == 1));
        const out = q.execute(window.rawData);

        const good = [];
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][0] % 2) {
                good.push(window.rawData[i]);
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.deep.equal(good);
    });

    it("Write a query", function () {
        const good1 = [];
        const good2 = [];
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][13].match(/THEFT/)) {
                good1.push(window.rawData[i]);
            }
            if (window.rawData[i][13].match(/^VEH-THEFT/)) {
                good2.push(window.rawData[i]);
            }
        }

        const out1 = theftsQuery.execute(window.rawData);
        const out2 = autoTheftsQuery.execute(window.rawData);

        chai.expect(out1).to.deep.equal(good1);
        chai.expect(out2).to.deep.equal(good2);
    });

    it("Add Apply and Count nodes", function () {
        function even(x) {
            return (x % 2) === 0;
        }

        const q = new ThenNode(
            new ApplyNode(x => x[0]),
            new ThenNode(
                new FilterNode(even),
                new CountNode()));
        const out = q.execute(window.rawData);

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            if (even(window.rawData[i][0])) good++;
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });

    it("Clean the data", function () {
        const out = cleanupQuery.execute(window.rawData);
        chai.expect(out).to.deep.equal(cleanData());
    });

    it("Implement a call-chaining interface", function () {
        const q = Q.apply(x => x[0] % 2)
            .filter(x => x)
            .count();
        const out = q.execute(window.rawData);

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][0] % 2) good++;
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });

    it("Reimplement queries with call-chaining", function () {
        const data = cleanData();
        const out = cleanupQuery2.execute(window.rawData);
        chai.expect(out).to.deep.equal(data);

        const good1 = [];
        const good2 = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].type.match(/THEFT/)) {
                good1.push(data[i]);
            }
            if (data[i].type.match(/^VEH-THEFT/)) {
                good2.push(data[i]);
            }
        }


        const out1 = theftsQuery2.execute(data);
        const out2 = autoTheftsQuery2.execute(data);
        chai.expect(out1).to.deep.equal(good1);
        chai.expect(out2).to.deep.equal(good2);
    });
});

describe("Problem 2", function () {
    it("Optimize filters", function () {
        const q = Q.filter(x => x).filter(x => x);
        const q2 = q.optimize();
        chai.expect(q2.type).to.equal("Then");
        chai.expect((q2 as ThenNode).first.type).to.equal("All");
        chai.expect((q2 as ThenNode).second.type).to.equal("Filter");
    });

    it("Internal node types and CountIf", function () {
        const q = Q.filter(x => x[0] % 2).count();
        const q2 = q.optimize();
        chai.expect(q2.type).to.equal("Then");
        chai.expect((q2 as ThenNode).first.type).to.equal("All");
        chai.expect((q2 as ThenNode).second.type).to.equal("CountIf");
    });
});

describe("Problem 3", function () {
    it("Cartesian products", function () {
        const q = new CartesianProductNode(new AllNode(), new AllNode());
        const out = q.execute(window.rawData);
        const l = window.rawData.length;
        chai.expect(out).to.have.length(l * l);
    });

    it("Joins", function () {
        const q = Q.join(Q, (l, r) => l[0] == r[0] - 1).apply(x => x[0]);
        const out = q.execute(window.rawData);
        const l = window.rawData.slice(1);
        chai.expect(out).to.deep.equal(l.map(x => x[0]));
    });

    it("Joins Objects", function () {
        const q = Q.join(Q, (l, r) => true);
        const data = [{a: 0, b: 2}];
        const expected = [{a: 0, b: 2}];

        chai.expect(q.execute(data)).to.deep.equal(expected);
        chai.expect(q.optimize().execute(data)).to.deep.equal(expected);
    });

    it("Optimizing joins", function () {
        const q = Q.join(Q, function (l, r) {
            return l[0] == r[0] - 1
        });
        const outQ = q.optimize();
        chai.expect(outQ.type).to.equal("Join");

        const out = outQ.execute(window.rawData);
        chai.expect(out).to.deep.equal(q.execute(window.rawData));
    });

    it("Joins on fields", function () {
        const q = Q.join(Q, "type").count();
        const out = q.execute(cleanData());

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            for (let j = 0; j < window.rawData.length; j++) {
                if (window.rawData[i][13] == window.rawData[j][13]) good++;
            }
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });

    it("Implement hash joins", function () {
        const q = new HashJoinNode("type", new AllNode(), new AllNode()).count();
        const out = q.execute(cleanData());

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            for (let j = 0; j < window.rawData.length; j++) {
                if (window.rawData[i][13] == window.rawData[j][13]) good++;
            }
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });

    it("Allows duplicate records in HashJoin", function () {
        const q = Q.join(Q, "a");
        const datum = {a: 3, b: 2};
        const data = [datum, datum];

        chai.expect(q.execute(data)).to.have.length(4);
        chai.expect(q.optimize().execute(data)).to.have.length(4);
    });

    it("Optimize joins on fields to hash joins", function () {
        const q = Q.join(Q, "type");
        const out = q.optimize();
        chai.expect(out.type).to.equal("HashJoin");
    });
});
