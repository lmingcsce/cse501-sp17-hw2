This directory contains the starter kit for CSE501 Homework 2, "Optimizing Query Engine"

You have the option of writing this assignment in JavaScript or TypeScript, but we will grade only one of them, so please let us know in the survey which implementation you would like us to grade.

For JavaScript:

- The query engine skeleton is in `src/q.js`
- To run the tests in `test/test.js`, open `test/q.html` in your browser

For TypeScript:

- The query engine skeleton is in `src/q.ts`
- Compile by running `tsc` in the `hw2` directory.
  This will generate `src/q.js`.  
  **Warning:** it _will_ overwrite any changes made to `src/q.js` if you've worked on a JavaScript implementation too.
- To run the tests in `test/test.js`, open `test/q.html` in your browser.
