- Your composed queries are a little verbose. You do not actually need `ThenNode` and `AllNode` in them, just the `FilterNode` on its own will work.
- Filter fusion is incorrect. In JavaScript, `function1 && function2` is NOT equivalent to `x => function1(x) && function2(x)`. The former simply returns true if both function objects are truthy (and in your case, they always are). The latter is what you want.
- Your optimizations should never mutate the original AST nodes, they should construct the new ones instead. When you mutate the original nodes, you create a complex graph of dangling references in memory. Imagine the following (correct) code for filter fusion:

        AddOptimization(ThenNode, function() {
            if (this.first.type == "Then" &&
                this.first.second.type == "Filter" &&
                this.second.type == "Filter") {
                return new FilterNode(x => {
                    var p1 = this.first.second.predicate;
                    var p2 = this.second.predicate;
                    return p1(x) && p2(x);
                })
            }
        })

  Now imagine that you have some other optimization on a `FilterNode` that mutates it. The order of applying optimizations in unpredictable. By the time we get to executing this freshly created `FilterNode`, the structure of `this.first.second` and `this.second` may change (because of the mutation). Thus, the predicate will fail to find `p1` and `p2` that it expected and will crash at runtime.
- `product()` is supposed to be unary, not binary. It should work in a call-chaining fashion, just like the rest of our operators: `l.product(r)`.
- Wrong names in the join/product results: expected `left` and `right` instead of `l` and `r`.
- When you conduct an optimization to introduce a `HashJoinNode`, you eliminate an `ApplyNode` on top and ignore the function to be applied. This is wrong, that `ApplyNode` may have nothing to do with the join pattern. You wrongly assume that it is always going to be your `inter_join` function, but it's possible for the programmer to call `Q.join(Q.apply(f), "field")`, and your optimization will break that query.
