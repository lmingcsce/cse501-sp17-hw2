/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
//// The AST
// This class represents all AST Nodes
var ASTNode = (function () {
    function ASTNode(type) {
        this.type = type;
    }
    ASTNode.prototype.execute = function (data) {
        throw new Error("Unimplemented AST node " + this.type);
    };
    ASTNode.prototype.optimize = function () {
        return this;
    };
    ASTNode.prototype.run = function (data) {
        return this.optimize().execute(data);
    };

    //// 1.5 call-chaining interface
    ASTNode.prototype.apply = function (func) {
        return new ThenNode(this, new ApplyNode(func)); 
    };
    ASTNode.prototype.filter = function (func) {
        return new ThenNode(this, new FilterNode(func)); 
    };
    ASTNode.prototype.count = function () {
        return new ThenNode(this, new CountNode());
    };
    ASTNode.prototype.product = function (node1, node2) {
        return new ThenNode(this, new CartesianProductNode(node1, node2));
    };
    ASTNode.prototype.join = function (node, func) {
        if (typeof(func) == "string") {
            var str = func;
            var new_func = function (x) {
                return x.l[str] == x.r[str];
            }
            new_func.field = str;
            return this.product(this, node).filter(new_func).apply(inter_join);
        }

        return this.product(this, node).filter(function(x) {return func(x.l, x.r);}).apply(inter_join);
    };

    return ASTNode;
}());

// The ALL node just outputs all records.
var AllNode = (function (_super) {
    __extends(AllNode, _super);
    function AllNode() {
        return _super.call(this, "All") || this;
    }

    //// 1.1 implement execute
    AllNode.prototype.execute = function (data) {
        var all_result = [];

        for (x of data){
            all_result.push(x);
        }

        return all_result;
    };

    return AllNode;
}(ASTNode));

// The Filter node uses a callback to throw out some records
var FilterNode = (function (_super) {
    __extends(FilterNode, _super);
    function FilterNode(predicate) {
        var _this = _super.call(this, "Filter") || this;
        _this.predicate = predicate;
        return _this;
    }

    //// 1.1 implement execute
    FilterNode.prototype.execute = function (data) {
        var filter_result = [];

        for (x of data) {
            if (this.predicate(x))
                filter_result.push(x);
        }

        return filter_result;
    }

    return FilterNode;
}(ASTNode));

// The Then node chains multiple actions on one data structure
var ThenNode = (function (_super) {
    __extends(ThenNode, _super);
    function ThenNode(first, second) {
        var _this = _super.call(this, "Then") || this;
        _this.first = first;
        _this.second = second;
        return _this;
    }

    //// 1.1 implement execute
    ThenNode.prototype.execute = function (data) {

        var first_result = [];
        var second_result = [];

        for (x of this.first.execute(data)) {
            first_result.push(x);
        }

        for (x of this.second.execute(first_result)){
            second_result.push(x);
        }

        return second_result;
    }

    ThenNode.prototype.optimize = function () {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    };
    return ThenNode;
}(ASTNode));

//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables
var theftsQuery = new ThenNode(new AllNode(), new FilterNode(function (x) { return x[13].indexOf("THEFT") >= 0; }));
var autoTheftsQuery = new ThenNode(new AllNode(), new FilterNode(function (x) { return x[13].indexOf("VEH-THEFT") >= 0; }));

//// 1.3 Add Apply and Count Nodes
var ApplyNode = (function (_super) {
    __extends(ApplyNode, _super);
    function ApplyNode(func) {
        var _this = _super.call(this, "Apply") || this;
        _this.func = func;
        return _this;
    }

    ApplyNode.prototype.execute = function (data) {
        var apply_result = [];

        for (x of data) {
            apply_result.push(this.func(x));
        }

        return apply_result;
    };

    return ApplyNode;
}(ASTNode));

var CountNode = (function (_super) {
    __extends(CountNode, _super);
    function CountNode() {
        return _super.call(this, "Count") || this;
    }

    CountNode.prototype.execute = function (data) {
        var count_result = [];

        count_result.push(data.length);

        return count_result;
    };

    return CountNode;
}(ASTNode));

var CountIfNode = (function (_super) {
    __extends(CountIfNode, _super);
    function CountIfNode(predicate) {
        var _this = _super.call(this, "CountIf") || this;
        _this.predicate = predicate;
        return _this;
    }

    CountIfNode.prototype.execute = function (data) {
        var filter_result = [];
        var count_result = [];

        for (x of data) {
            if (this.predicate(x))
                filter_result.push(x);
        }

        count_result.push(filter_result.length);

        return count_result;
    };

    return CountIfNode;
}(ASTNode));

//// 1.4 Clean the data
var cleanupQuery = new ApplyNode(function(x){return { type: x[13], description: x[15], date: x[17], area: x[19] };});

//// 1.5 Implement a call-chaining interface
var Q = new AllNode();

//// 1.6 Reimplement queries with call-chaining
var cleanupQuery2 = Q.apply(function(x) {return {type: x[13], description: x[15], date: x[17], area: x[19] };}); // ...
var theftsQuery2 = Q.filter(function(x) {return x.type.indexOf("THEFT") >= 0; }); 
var autoTheftsQuery2 = Q.filter(function(x) {return x.type.indexOf("VEH-THEFT") >= 0; });

//// 2.1 Optimize Queries
function AddOptimization(nodeType, opt) {
    var old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function () {
        var newThis = old.call(this);
        return opt.call(newThis) || newThis;
    };
}

AddOptimization(ThenNode, function(){
    if ((this.first.type == "Then") && (this.first.second.type == "Filter") && (this.second.type == "Filter")) {
        var pred1 = this.first.second.predicate;
        var pred2 = this.second.predicate;

        this.first = this.first.first;
        this.second = new FilterNode(pred1 && pred2);
    }
});

//// 2.2 Internal node types and CountIf
AddOptimization(ThenNode, function(){
    if ((this.first.type == "Then") && (this.first.second.type == "Filter") && (this.second.type == "Count")) {
        var pred = this.first.second.predicate;

        this.first = this.first.first;
        this.second = new CountIfNode(pred);
    }
});

//// 3.1 Cartesian Products
var CartesianProductNode = (function (_super) {
    __extends(CartesianProductNode, _super);
    function CartesianProductNode(first, second) {
        var _this = _super.call(this, "CartesianProduct") || this;
        _this.first = first;
        _this.second = second;
        return _this;
    }

    CartesianProductNode.prototype.execute = function (data) {
        var first_result = this.first.execute(data);
        var second_result = this.second.execute(data);
        var cartesian_product_result = [];

        for (x of first_result) {
            for (y of second_result) {
                cartesian_product_result.push({l:x, r:y});
            }
        }

        return cartesian_product_result;
    };

    return CartesianProductNode;
}(ASTNode));

//// 3.2 Joins
function inter_join(x) {
    var l_var = x.l;
    var r_var = x.r;
    var inter_join_result = {};

    for (i in l_var)
        inter_join_result[i] = l_var[i];

    for (i in r_var)
        inter_join_result[i] = r_var[i];

    return inter_join_result;
}

//// 3.3 Optimizing joins
var JoinNode = (function (_super) {
    __extends(JoinNode, _super);
    function JoinNode(first, second, func) {
        var _this = _super.call(this, "Join") || this;
        _this.first = first;
        _this.second = second;
        _this.func = func;
        return _this;
    }

    JoinNode.prototype.execute = function (data) {
        var first_result = this.first.execute(data);
        var second_result = this.second.execute(data);
        var product_result = [];
        var filter_result = [];
        var join_result = [];

        for (x of first_result) {
            for (y of second_result) {
                product_result.push({l:x, r:y});
            }
        }

        for (x of product_result) {
            if (this.func(x)) {
                filter_result.push(x);
            }
        }

        for (x of filter_result) {
            join_result.push(inter_join(x));
        }

        return join_result;
    };

    return JoinNode;
}(ASTNode));

AddOptimization(ThenNode, function(){
    if ((this.first.type == "Then") && (this.second.type == "Apply")) {
        if ((this.first.first.type == "Then") && 
            (this.first.second.type == "Filter")) {
            if ((this.first.first.first.type == "All") && 
                (this.first.first.second.type == "CartesianProduct")) {
                var first_var = this.first.first.second.first;
                var second_var = this.first.first.second.first;
                var third_var = this.first.second.predicate;

                if ("field" in this.first.second.predicate) {
                    var new_func = this.first.second.predicate;
                    return new HashJoinNode(new_func.field, first_var, second_var);
                }

                return new JoinNode(first_var, second_var, third_var);
            }
        }
    }
});

//// 3.4 Join on fields
// See above

//// 3.5 Implement hash joins
var HashJoinNode = (function (_super) {
    __extends(HashJoinNode, _super);
    function HashJoinNode(field, first, second) {
        var _this = _super.call(this, "HashJoin") || this;
        _this.field = field;
        _this.first = first;
        _this.second = second;
        return _this;
    }

    HashJoinNode.prototype.execute = function (data) {
        var first_result = this.first.execute(data);
        var second_result = this.second.execute(data);
        var first_ht = {};
        var second_ht = {};
        var hash_join_result = [];

        for (x of first_result) {
            var tmp_field = x[this.field];
            if (tmp_field in first_ht) {
                first_ht[tmp_field].push(x);
            } else {
                first_ht[tmp_field] = [x];
            }
        }

        for (y of second_result) {
            var tmp_field = y[this.field];
            if (tmp_field in second_ht) {
                second_ht[tmp_field].push(y);
            } else {
                second_ht[tmp_field] = [y];
            }
        }

        for (var key in first_ht) {
            if (key in second_ht) {
                var tmp_product_result = [];

                for (x of first_ht[key]) {
                    for (y of second_ht[key]) {
                        tmp_product_result.push({l:x, r:y});
                    }
                }
                hash_join_result = hash_join_result.concat(tmp_product_result);
            }
        }

        return hash_join_result.map(inter_join);
    };

    return HashJoinNode;
}(ASTNode));

//// 3.6 Optimize joins on fields to hash joins
// See above
//# sourceMappingURL=q.js.map
