/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */

//// The AST

// This class represents all AST Nodes
class ASTNode {
    type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Unimplemented AST node " + this.type);
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 Implement call-chaining
}

// The ALL node just outputs all records.
class AllNode extends ASTNode {

    constructor() {
        super("All");
    }

    //// 1.1 implement execute
}

// The Filter node uses a callback to throw out some records
class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
}

// The Then node chains multiple actions on one data structure
class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}


//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables

let theftsQuery = new ASTNode("...");
let autoTheftsQuery = new ASTNode("...");

//// 1.3 Add Apply and Count Nodes
// ...

//// 1.4 Clean the data

let cleanupQuery = new ASTNode("...");

let Q = new AllNode();

//// 1.6 Reimplement queries with call-chaining

let cleanupQuery2 = Q; // ...

let theftsQuery2 = Q; // ...

let autoTheftsQuery2 = Q; // ...

//// 2.1 Optimize Queries

function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    let old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
        let newThis = old.call(this);
        return opt.call(newThis) || newThis;
    }
}

// ...

//// 2.2 Internal node types and CountIf
// ...

//// 3.1 Cartesian Products
// ...

//// 3.2 Joins
// ...

//// 3.3 Optimizing joins
// ...

//// 3.4 Join on fields
// ...

//// 3.5 Implement hash joins
// ...

//// 3.6 Optimize joins on fields to hash joins
// ...
